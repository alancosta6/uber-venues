package com.uber.interview.util;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import androidx.annotation.RawRes;

public class Util {

    private static final String TAG = Util.class.getSimpleName();
    private static final String UTF_8 = "UTF-8";

    public static <T> T getJsonMock(Context context, @RawRes int rawId, Class<T> classType) {
        InputStream is = context.getResources().openRawResource(rawId);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, UTF_8));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        String jsonString = writer.toString();
        Gson g = new Gson();
        return g.fromJson(jsonString, classType);
    }

}

package com.uber.interview.network;

public interface ApiResponseCB<T> {
    void onSuccess(T success);
    void onFailure();
}

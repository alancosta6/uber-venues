package com.uber.interview.venues.models;

import java.io.Serializable;

public class VenuesDTO implements Serializable {

    private VenuesResponseDTO response;

    public VenuesResponseDTO getResponse() {
        return response;
    }

    public void setResponse(VenuesResponseDTO response) {
        this.response = response;
    }
}

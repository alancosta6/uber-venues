package com.uber.interview.venues;

import android.util.Log;

import com.uber.interview.network.ApiResponseCB;
import com.uber.interview.venues.api.VenuesAPI;
import com.uber.interview.venues.models.VenueDTO;
import com.uber.interview.venues.models.VenuesDTO;
import com.uber.interview.venues.models.VenuesRequestDTO;
import com.uber.interview.venues.models.VenuesResponseDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VenuesModel implements VenuesContract.Model {

    private static String TAG = VenuesModel.class.getSimpleName();
    private Call<VenuesDTO> venuesCall;

    @Override
    public void loadVenues(VenuesRequestDTO request, final ApiResponseCB<List<VenueDTO>> cb) {

        if(request == null || cb == null) {
            Log.e(TAG, "Params must not be null" );
            return;
        }

        String lat = request.getLatitude();
        if(lat == null || lat.isEmpty()) {
            Log.e(TAG, "must have latitude to make request" );
            cb.onFailure();
            return;
        }

        String lon = request.getLongitude();
        if(lon == null || lon.isEmpty()) {
            Log.e(TAG, "must have longitude to make request" );
            cb.onFailure();
            return;
        }

        String latLong = lat.concat(",").concat(lon);
        String query = request.getQuery() == null ? "" : request.getQuery();



        venuesCall = VenuesAPI.service.loadVenues(latLong, query);

        venuesCall.enqueue(new Callback<VenuesDTO>() {
            @Override
            public void onResponse(Call<VenuesDTO> call, Response<VenuesDTO> response) {

                if(response.isSuccessful() ) {

                    boolean hasVenues = false;
                    VenuesDTO mVenues = response.body();
                    if(mVenues != null) {
                        VenuesResponseDTO venuesResp = mVenues.getResponse();
                        if(venuesResp != null) {
                            cb.onSuccess(venuesResp.getVenues());
                            hasVenues = true;
                        }
                    }

                    if(!hasVenues) {
                        cb.onFailure();
                    }
                } else {
                    cb.onFailure();
                }
            }

            @Override
            public void onFailure(Call<VenuesDTO> call, Throwable t) {

                if(t.getMessage().toLowerCase().equals("canceled")) {
                    return;
                }
                cb.onFailure();
            }
        });
    }

    @Override
    public void cancelVenuesRequest() {
        if(venuesCall != null) {
            venuesCall.cancel();
        }
    }
}

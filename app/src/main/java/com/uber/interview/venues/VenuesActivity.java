package com.uber.interview.venues;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.uber.interview.R;
import com.uber.interview.venues.models.VenueDTO;

import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class VenuesActivity extends AppCompatActivity implements VenuesContract.View {

    private static final String TAG = VenuesActivity.class.getSimpleName();

    private static final int PERMISSIONS_REQUEST_LOCATION = 1001;
    private static final long LOCATION_REFRESH_TIME_MS = TimeUnit.MINUTES.toMillis(0);
    private static final float LOCATION_REFRESH_DISTANCE = 500;

    private LocationManager mLocationManager;
    private VenuesContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venues);
        presenter = new VenuesPresenter(this);
        onQueryChanged();

        if (!hasLocationPermission()) {
            requestLocationPermission();
        } else {
            startLocationListener();
        }
    }

    @Override
    public void setPresenter(VenuesContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLoading(boolean isActive) {
        findViewById(R.id.venues_progressbar).setVisibility(isActive ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onQueryChanged() {

        final LocationManager mLocationManager = getLocationManager();

        ((EditText) findViewById(R.id.venues_query)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @SuppressLint("MissingPermission")
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                double lat = 0;
                double lng = 0;
                if(hasLocationPermission()) {
                    Location location = mLocationManager.getLastKnownLocation(geLocationProvider());
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                }

                presenter.loadVenues(charSequence.toString(), lat, lng);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void showVenues(List<VenueDTO> venues) {

        if (venues == null || venues.isEmpty()) {
            return;
        }

        RecyclerView mVenuesRecycler = findViewById(R.id.venues_recycler);
        mVenuesRecycler.setAdapter(new VenuesAdapter(venues));
    }

    @Override
    public void showError() {
        Toast.makeText(this, "ERROR", Toast.LENGTH_LONG).show();
    }

    @Override
    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION);
    }

    @Override
    public boolean hasLocationPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void startLocationListener() {

        LocationListener mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(TAG, "onLocationChanged");
                presenter.loadVenues(getQuery(), location.getLatitude(), location.getLongitude());
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.d(TAG, "onStatusChanged");
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.d(TAG, "onProviderEnabled");
            }

            @Override
            public void onProviderDisabled(String s) {
                Log.d(TAG, "onProviderDisabled");
            }
        };

        if (hasLocationPermission()) {
            getLocationManager().requestLocationUpdates(geLocationProvider(), LOCATION_REFRESH_TIME_MS, LOCATION_REFRESH_DISTANCE, mLocationListener);
        }

    }


    private LocationManager getLocationManager() {

        if(mLocationManager == null) {
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        }

        return mLocationManager;
    }


    private String geLocationProvider() {

        boolean isGpsLocationEnabled = getLocationManager().isProviderEnabled(LocationManager.GPS_PROVIDER);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(isGpsLocationEnabled ? Criteria.ACCURACY_FINE : Criteria.ACCURACY_COARSE);
        return getLocationManager().getBestProvider(criteria, true);
    }

    @Override
    public String getQuery() {
        return ((EditText) findViewById(R.id.venues_query)).getText().toString();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationListener();
                }
                return;
            }
        }
    }
}

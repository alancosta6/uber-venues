package com.uber.interview.venues.models;

import java.io.Serializable;
import java.util.List;

public class VenueDTO implements Serializable {


    private String name;
    private VenueContactDTO contact;
    private List<VenueCategoryDTO> categories;
    private VenueLocationDTO location;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VenueContactDTO getContact() {
        return contact;
    }

    public void setContact(VenueContactDTO contact) {
        this.contact = contact;
    }

    public List<VenueCategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<VenueCategoryDTO> categories) {
        this.categories = categories;
    }

    public VenueLocationDTO getLocation() {
        return location;
    }

    public void setLocation(VenueLocationDTO location) {
        this.location = location;
    }

}

package com.uber.interview.venues.api;

import com.uber.interview.venues.models.VenuesDTO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface VenuesServiceAPI {

    @GET("search?radius=1000&oauth_token=V3IN04VHVFKVAB1OKESONN31NGPWM0JHP4S0ODM1XMP1YCI5&intent=browse&v=20170811")
    Call<VenuesDTO> loadVenues(@Query("ll") String latLong, @Query("query") String query);
}

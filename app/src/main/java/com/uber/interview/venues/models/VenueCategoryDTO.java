package com.uber.interview.venues.models;

import java.io.Serializable;

public class VenueCategoryDTO implements Serializable {

    private VenueCategoryIconDTO icon;

    public VenueCategoryIconDTO getIcon() {
        return icon;
    }

    public void setIcon(VenueCategoryIconDTO icon) {
        this.icon = icon;
    }

}

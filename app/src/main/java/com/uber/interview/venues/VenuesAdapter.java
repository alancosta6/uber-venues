package com.uber.interview.venues;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.uber.interview.R;
import com.uber.interview.venues.models.VenueCategoryDTO;
import com.uber.interview.venues.models.VenueCategoryIconDTO;
import com.uber.interview.venues.models.VenueContactDTO;
import com.uber.interview.venues.models.VenueDTO;
import com.uber.interview.venues.models.VenueLocationDTO;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.ViewHolder> {

    private String TAG = VenuesAdapter.class.getSimpleName();
    private List<VenueDTO> mVenues;


    VenuesAdapter(List<VenueDTO> mVenues) {
        this.mVenues = mVenues;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(inflater.inflate(R.layout.list_item_venues, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        VenueDTO venue = mVenues.get(position);
        if(venue == null) {
            return;
        }
        holder.venueName.setText(venue.getName() != null ? venue.getName() : "");

        VenueContactDTO contact = venue.getContact();
        holder.venueContact.setText( contact.getFormattedPhone() != null ? contact.getFormattedPhone() : "");

        VenueLocationDTO location = venue.getLocation();
        if(location != null && location.getFormattedAddress().size() > 0) {
            holder.venueAddress.setText(location.getFormattedAddress().get(0));
        } else {
            holder.venueAddress.setText("");
        }


        List<VenueCategoryDTO> venueCategories = venue.getCategories();
        if(venueCategories != null && venueCategories.size() > 0) {

            VenueCategoryIconDTO icon = venueCategories.get(0).getIcon();
            Glide.with(holder.venueImage)
                    .load(icon.getPrefix().concat("64").concat(icon.getSuffix()))
                    .into(holder.venueImage);
        }





    }

    @Override
    public int getItemCount() {
        return mVenues != null ? mVenues.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView venueImage;
        TextView venueName;
        TextView venueContact;
        TextView venueAddress;

        ViewHolder(View view) {
            super(view);
            venueImage = view.findViewById(R.id.venue_image);
            venueName = view.findViewById(R.id.venue_name);
            venueContact = view.findViewById(R.id.venue_phone);
            venueAddress = view.findViewById(R.id.venue_address);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Context ctx = view.getContext();
            if(ctx == null) {
                Log.e(TAG, "Cant open google maps without context");
                return;
            }

            int adapterPosition = getAdapterPosition();
            if(mVenues == null || mVenues.size() < adapterPosition) {
                Log.e(TAG, "Cant get venue at position:" + adapterPosition);
                return;
            }

            VenueDTO venue = mVenues.get(adapterPosition);
            VenueLocationDTO venueLocation = venue.getLocation();

            String lat = Double.toString(venueLocation.getLat());
            String lng = Double.toString(venueLocation.getLng());
            String query = lat.concat(",").concat(lng);

            Uri gmmIntentUri = Uri.parse("google.navigation:q=".concat(query));
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(ctx.getPackageManager()) != null) {
                ctx.startActivity(mapIntent);
            }
        }
    }


}

package com.uber.interview.venues.models;

import java.io.Serializable;

public class VenueContactDTO implements Serializable {


    private String phone;
    private String formattedPhone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFormattedPhone() {
        return formattedPhone;
    }

    public void setFormattedPhone(String formattedPhone) {
        this.formattedPhone = formattedPhone;
    }



}

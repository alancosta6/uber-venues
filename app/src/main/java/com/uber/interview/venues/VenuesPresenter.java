package com.uber.interview.venues;

import android.util.Log;

import com.uber.interview.network.ApiResponseCB;
import com.uber.interview.venues.models.VenueDTO;
import com.uber.interview.venues.models.VenuesRequestDTO;

import java.util.List;

class VenuesPresenter implements VenuesContract.Presenter {

    private static final String TAG =  VenuesPresenter.class.getSimpleName();

    private VenuesContract.View mView;
    private VenuesContract.Model mModel;


    VenuesPresenter(VenuesContract.View mView) {
        this.mView = mView;
        this.mModel = new VenuesModel();
    }


    @Override
    public void loadVenues(String query, double latitude, double longitude) {


        if(query == null || query.isEmpty()) {
            return;
        }

        mView.showLoading(true);

        VenuesRequestDTO request = new VenuesRequestDTO();
        request.setLatitude(Double.toString(latitude));
        request.setLongitude(Double.toString(longitude));
        request.setQuery(query);

        Log.d(TAG, "Lodading venues for Query: " + query + " Latitude: " + latitude + " Longitude:" + longitude);

        mModel.cancelVenuesRequest();
        mModel.loadVenues(request, new ApiResponseCB<List<VenueDTO>>() {
            @Override
            public void onSuccess(List<VenueDTO> success) {
                mView.showVenues(success);
                mView.showLoading(false);
            }

            @Override
            public void onFailure() {
                mView.showError();
                mView.showLoading(false);
            }
        });
    }

}

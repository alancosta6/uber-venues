package com.uber.interview.venues.models;

import java.io.Serializable;
import java.util.List;

public class VenueLocationDTO implements Serializable {

    private List<String> formattedAddress;
    private double lat;
    private double lng;

    public List<String> getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(List<String> formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}

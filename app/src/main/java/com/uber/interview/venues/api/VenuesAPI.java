package com.uber.interview.venues.api;

import com.uber.interview.network.BaseApi;

public class VenuesAPI extends BaseApi {

    private static String VENUES_BASE_URL = "https://api.foursquare.com/v2/venues/";
    public static VenuesServiceAPI service;

    @Override
    public String setBaseUrl() {
        return VENUES_BASE_URL;
    }


    private static final VenuesAPI instance = new VenuesAPI();

    public static VenuesAPI getInstance() {
        return instance;
    }


    VenuesAPI() {
        service = retrofit.create(VenuesServiceAPI.class);
    }
}

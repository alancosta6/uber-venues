package com.uber.interview.venues.models;

import java.io.Serializable;
import java.util.List;

public class VenuesResponseDTO implements Serializable {

    private List<VenueDTO> venues;

    public List<VenueDTO> getVenues() {
        return venues;
    }

    public void setVenues(List<VenueDTO> venues) {
        this.venues = venues;
    }

}

package com.uber.interview.venues;

import com.uber.interview.BaseModel;
import com.uber.interview.BasePresenter;
import com.uber.interview.BaseView;
import com.uber.interview.network.ApiResponseCB;
import com.uber.interview.venues.models.VenueDTO;
import com.uber.interview.venues.models.VenuesRequestDTO;

import java.util.List;

interface VenuesContract {

    interface View extends BaseView<Presenter> {
        void showLoading(boolean isActive);
        void onQueryChanged();
        void showVenues(List<VenueDTO> venues);
        void showError();
        void requestLocationPermission();
        boolean hasLocationPermission();
        void startLocationListener();
        String getQuery();
    }

    interface Presenter extends BasePresenter {
        void loadVenues(String query, double latitude, double longitude);
    }

    interface Model extends BaseModel {
        void loadVenues(VenuesRequestDTO request, ApiResponseCB<List<VenueDTO>> cb);
        void cancelVenuesRequest();
    }
}

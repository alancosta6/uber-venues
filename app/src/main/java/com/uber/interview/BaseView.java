package com.uber.interview;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
